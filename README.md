# Systems Biology for Tissue Engineering Students
## Overview
Systems Biology is a field of biological science that aims to integrate all possible information related to molecules and their interactions as a network in various levels (organism, tissue, or living cell). This approach employs tools initially established in mathematics, physics, chemistry, and computer sciences. Systems Biology goal is to obtain some results which are not easily accessible solely based on reductionism methods with the currently available molecular biology tools. During this course, we are going to be familiar with some basics concepts and methods used in the Systems Biology approach.
## Instructors: 
Dr. Yazdan Asgari ([Webpage](https://yazdan59.github.io/))
## Time & Location: 
Sep-Dec 2019, lectures are held on Tuesdays 3:00 to 5:00 PM at Class#3, School of Advanced Technologies in Medicine, Tehran University of Medical Sciences
## Exam
The Score for this course would be taken through several parts including Take Home Exams for every session, some Assignments during the semester, Student' Seminars, and a Final Exam.
## Lecture Schedule
| # | Date | Title | Description | Instructor |Reference |
| --- | --- | --- | --- | --- | --- |
| 1 | 99/6/25 | Introduction | Some Information about Course Contents, References, Exam Strategies, System definition in different contexts | Dr. Asgari | Klipp, et al., Systems Biology in Practice,  John Wiley & Sons, 2005, Chapter 1<br>([Slide](slides/01_Introduction.pdf)) |
| 2 | 99/7/1 | Students’ Seminar | -- | Dr. Asgari | ([Paper](refs/2013-Systems_biology_characterization_of_engineered_tissues.pdf)) |
| 3 | 99/7/8 | Biological Networks, Graph Theory | Learning About Global and local properties such as degree, clustering coefficients, motif, graph representation of biological networks | Dr. Asgari | Junker, Schreiber, Analysis of Biological Networks,  John Wiley & Sons, 2008, Chapters 2 to 8, 10, and 13<br>([Slide1](slides/03_Biological_Networks.pdf))<br>([Slide2](slides/03_Graph_Theory.pdf)) |
| 4 | 99/7/15 | Structural Analysis  | Working with Cytoscape, Basics | Dr. Asgari | Shannon, et al., [Cytoscape: a software environment for integrated models of biomolecular interaction networks](http://www.ncbi.nlm.nih.gov/pubmed/14597658), Genome Res., 2003, 13(11), 2498-504 |
| 5 | 99/7/22 | Structural Analysis | Network Reconstruction from Databases, Finding Modules, Motif, Gene Ontology<br>([Assignment#1](assignment/Assignment1.pdf)) | Dr. Asgari | [Cytoscape App Store](http://apps.cytoscape.org/)<br>([Slide](slides/07_Gene_Ontology.pdf)) |
| 6 | 99/7/29 | Structural Analysis | Literature Review | Dr. Asgari | Jeong, Tombor, Albert, Oltvai, Barabasi, [The large-scale organization of metabolic networks](http://www.ncbi.nlm.nih.gov/pubmed/11034217), Nature, 2000, 407(6804), 651-4<br> Ravasz, Somera, Mongru, Oltvai, Barabasi, [Hierarchical organization of modularity in metabolic networks](http://www.ncbi.nlm.nih.gov/pubmed/12202830), Science, 2002, 297(5586), 1551-5 |
| 7 | 99/8/6 | Finding Differentially Expressed Genes | Working with geWorkbench<br>([Assignment#2](assignment/Assignment2.pdf)) | Dr. Asgari | Brazma, Vilo, [Gene expression data analysis](https://www.ncbi.nlm.nih.gov/pubmed/10967323), FEBS Lett. 2000, 480(1), 17-24<br>[geWorkbench](http://wiki.c2b2.columbia.edu/workbench/index.php/Home) |
| 8 | 99/8/20 | Literature Review | Some ideas about Tissue Engineering & Systems Biology Approach | Dr. Asgari | -- |
| 9 | 99/8/27 | Network Dynamics | Learning About Kinetic Modeling Approach; its advantages and challenges | Dr. Asgari | Enzyme Kinetics, Chapter 10<br>Huang, et al., [Ultrasensitivity in the mitogen-activated protein kinase cascade](http://www.ncbi.nlm.nih.gov/pubmed/8816754), PNAS, 1996, 93(19), 10078-83<br>Nelson, et al., [Oscillations in NF-kappaB signaling control the dynamics of gene expression](http://www.ncbi.nlm.nih.gov/pubmed/15499023), Science, 2004, 306(5696), 704-8<br>([Slide](slides/13_Kinetic_Modeling.pdf)) |
| 10 | 99/9/4 | Students’ Seminars | -- | Dr. Asgari | -- |
